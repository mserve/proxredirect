# Proxmox redirect service. 

This little service cracefully redirects to
default PVE port 8006. HTTPS uses the default PVE
certicates stored in /etc/pve/local

Usage:

`proxredirect.py --mode=<mode>`

  `<mode>`: can be `http` or `https` (http by default)


## Set up as service with systemd

To enable service, set proper path in `.service` files and 
copy them to `/etc/systemd/system/`

```
systemctl daemon-reload
systemctl enable proxredirect-http
systemctl enable proxredirect-https
systemctl start  proxredirect-http
systemctl start  proxredirect-https
```
