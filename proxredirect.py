#!/usr/bin/python3

from http.server import BaseHTTPRequestHandler, HTTPServer
import ssl, getopt, sys

# HTTPRequestHandler class
class redirectHandler(BaseHTTPRequestHandler):

  # GET
  def do_GET(self):
        # Send response status code
        self.send_response(301)
        self.send_header('Location','https://{}:8006'.format(self.headers.get('Host')))
        # Send headers
        self.end_headers()
        return

def run_http():
  print('starting HTTP server...')

  # Server settings
  server_address = ('', 80)
  httpd = HTTPServer(server_address, redirectHandler)
  print('running HTTP server...')
  httpd.serve_forever()

def run_https():
  print('starting HTTPS server...')
  server_address = ('', 443)
  httpsd = HTTPServer(server_address, redirectHandler)
  httpsd.socket = ssl.wrap_socket(httpsd.socket,
                               server_side=True,
                               certfile='/etc/pve/local/pve-ssl.pem',
                               keyfile='/etc/pve/local/pve-ssl.key')
  print('running HTTPS server...')
  httpsd.serve_forever()

def main(argv):
  mode='http'
  try:
    opts, args = getopt.getopt(argv, "m:", ["mode="])
  except getoptGetoptError:
    print('proxredirect-py [-m <mode>]')
    sys.exit()
  for opt, arg in opts:
    if opt == '-h':
      print('proxredirect-py [-m <mode>]')
      print('  <mode>: http or https (http by default)')
    elif opt in ('-m', '--mode'):
      if arg.lower() in ('http', 'https'):
        mode = arg.lower()
      else:
        print('unknown mode: {}. Allowed modes: http or https'.format(arg))
  if mode == 'https':
    run_https()
  else:
    run_http()

if __name__ == "__main__":
   main(sys.argv[1:])
